import java.util.HashMap;
import java.util.HashSet;

// Viide: http://www.kosbie.net/cmu/fall-09/15-110/handouts/recursion/Cryptarithms.java

public class Puzzle {
	public static void permute(int n, int k) {
		permute(n, k, new HashSet<Integer>(), new int[k]);
	}

	public static void permute(int n, int k, HashSet<Integer> set, int[] permutatsioon) {
		if (set.size() == k)
			doPermute(n, k, set, permutatsioon);
		else {
			for (int i = 0; i < n; i++)
				if (!set.contains(i)) {
					permutatsioon[set.size()] = i;
					set.add(i);
					permute(n, k, set, permutatsioon);
					set.remove(i);
				}
		}
	}

	public static void doPermute(int n, int k, HashSet<Integer> set, int[] permutatsioon) {
		HashMap<Character, Integer> charmap = new HashMap<Character, Integer>();
		
		for (int i = 0; i < k; i++)
			charmap.put(t2hed.charAt(i), 
					permutatsioon[i]);
		long[] v22rtused = new long[3];
		for (int j = 0; j < 3; j++) {
			String s6na = s6ned[j];
			if (charmap.get(s6na.charAt(0)) == 0)
				return;
			long v22rtus = 0;
			for (int i = 0; i < s6na.length(); i++)
				v22rtus = 10 * v22rtus + charmap.get(s6na.charAt(i));
			v22rtused[j] = v22rtus;
		}
		if (v22rtused[0] + v22rtused[1] == v22rtused[2]) {
			System.out.println(charmap);
			System.out.println(s6ned[0] + " + " + s6ned[1] + " = " + s6ned[2]);
			System.out.println(v22rtused[0] + " + " + v22rtused[1] + " = " + v22rtused[2]);
			System.out.println();
			System.out.println();
			count++;
		}
	}

	private static String t2hed;
	private static String[] s6ned;
	private static int count = 0;

	
	public static void main(String[] args) {
		if(args.length != 3){
			 throw new RuntimeException("Sisendiks peab olema kolm s�net, aga on: " + args.length);
		}
		for (String s : args){
	         if(s.length() > 18)
	            throw new RuntimeException("S�ne pikkus on suurem kui 18: " +s);
	         if(s.length() <= 0)
	        	 throw new RuntimeException("Sisend on t�hi: " +s);
	         for (char c : s.toCharArray()){
	            if(!Character.isLetter(c))
	               throw new RuntimeException("S�nes ei ole ainult t�hed: " +s);
	         }
	      }
		s6ned = new String[3];
		s6ned[0] = args[0];
		s6ned[1] = args[1];
		s6ned[2] = args[2];
		String k6ik = args[0] + args[1] + args[2];
		t2hed = "";
		for (int i = 0; i < k6ik.length(); i++) {
			char c = k6ik.charAt(i);
			if (t2hed.indexOf(c) < 0)
				t2hed += c;
		}
		permute(10, t2hed.length());
		System.out.println("Lahendite arv: " + count);
		System.out.println("------------------------");
		System.out.println();
		count = 0;
	}
}